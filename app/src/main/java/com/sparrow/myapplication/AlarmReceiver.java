package com.sparrow.myapplication;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import java.util.Timer;
import java.util.TimerTask;

public class AlarmReceiver extends BroadcastReceiver {
    int j;
    MediaPlayer mp;
    @Override
    public void onReceive(final Context context, Intent intent)
    {
        Log.d("Autostart", "BOOT_COMPLETED broadcast received. Executing starter service.");

        Intent I = new Intent(context, AlarmService.class);
        context.startService(I);
        // For our recurring task, we'll just display a message
        Toast.makeText(context, "I'm running", Toast.LENGTH_LONG).show();
        Log.d("TAG", "onReceive: i m run");
     //   generateNotification(context);
        mp=MediaPlayer.create(context, R.raw.soft_alarm);
        mp.start();
//        Timer t = new Timer();
//
////Set the schedule function and rate
//        t.schedule(new TimerTask() {
//
//                       @Override
//                       public void run() {
//                           Log.d("ME", "Notification started");
//
//                           NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
//                        //   mBuilder.setSmallIcon(R.drawable.ddc);
//                           mBuilder.setContentTitle("My notification");
//                           mBuilder.setDefaults(Notification.DEFAULT_SOUND);
//                           mBuilder.setContentText("Hello World!");
//
//                           NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//                           mNotificationManager.notify(j++, mBuilder.build());
//
//                       }
//
//                   }, 0, 30000);
        //restartJobScheduler(context);
        if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.O) restartJobScheduler(context);
        else restartService(context);

    }

    private void restartJobScheduler(Context context)
    {
        Log.i("MyBroadCastReceiver", "onReceive");
        //context.startForegroundService(service);
//        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
//        Job myJob = dispatcher.newJobBuilder()
//                .setService(MyJobService.class)
//                .setTag("myFCMJob")
//                .build();
//        dispatcher.mustSchedule(myJob);
    }

    private void restartService(Context context)
    {
        Intent restartServiceIntent = new Intent(context, AlarmService.class);
        context.startService(restartServiceIntent);

    }}
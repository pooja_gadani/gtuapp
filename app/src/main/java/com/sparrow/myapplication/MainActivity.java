package com.sparrow.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent alarmIntent = new Intent(this, AlarmService.class);
        startService(alarmIntent);
    }
//    @Override
//    public void onDestroy() {
//        Intent alarmIntent = new Intent(this, AlarmService.class);
//        startService(alarmIntent);
//        super.onDestroy();
//    }

}
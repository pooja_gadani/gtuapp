package com.sparrow.myapplication;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.Calendar;

public class AlarmService extends Service {

    private String TAG ="AlarmService";
    @Override
    public void onCreate()
    {
        sendBroadcast();
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.e(TAG, "onStartCommand");
        //return super.onStartCommand(intent, flags, startId);
        return Service.START_STICKY;
    }


    @Override
    public void onDestroy() {
        sendBroadcast();
        super.onDestroy();
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onTaskRemoved(Intent rootIntent) {
        sendBroadcast();
        super.onTaskRemoved(rootIntent);
    }

    private void sendBroadcast()
    {
        Intent intent = new Intent(this,AlarmReceiver.class);

        //intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this.getApplicationContext(), 234324243, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
        Log.d("TAG", "sendBroadcast: "+  System.currentTimeMillis());
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 14);
        calendar.set(Calendar.MINUTE, 45);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);


        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        Log.d(TAG, "sendBroadcast: "+hour);
        if (alarmManager != null)
        {
            //System.currentTimeMillis() + (i * 100),
            alarmManager.setRepeating(
                    AlarmManager.RTC_WAKEUP,
                    System.currentTimeMillis(),
                    18000000,
                    pendingIntent);
        }
    }}